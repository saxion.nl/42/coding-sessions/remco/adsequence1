from scene import Scene
from gamelib import SpriteEntity, LabelEntity, Game
import math


window_width = 400
window_height = 400


class Background(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, "background.png", -1)
        self.x = window_width / 2
        self.y = window_height / 2
        self.scale = 0.5


class Cannon(SpriteEntity):
    def __init__(self, game, rotation, x, y):
        super().__init__(game, 'daniil_cannon.png')
        self.x = x
        self.y = y
        self.rotation = rotation
        self.scale = 0.15

    
    def update(self, dt):
        if self.rotation != -90:
            self.rotation -= 1


class Beer(SpriteEntity):
    def __init__(self, game, rotation, x, y):
        super().__init__(game, 'beer.png')
        self.x = x
        self.y = y
        self.rotation = 180
        self.scale = 0.04

        self.__x_speed = 0 # pixels/second
        self.__y_speed = 0 # pixels/second
        self.seconds = 0

    def update(self, dt):
        self.seconds = self.seconds +  dt
        # print(self.seconds)



        if self.seconds > 2.5:

            thrust = 400 * dt
        
            radians = (self.rotation + 90) / 360 * 2 * math.pi

            self.__x_speed += thrust * math.sin(radians)
            self.__y_speed += thrust * math.cos(radians)

            self.x += self.__x_speed * dt
            self.y += self.__y_speed * dt


    
if __name__ == "__main__":
    class DaniilGame(Game):
        def __init__(self):
            super().__init__(window_width, window_height)
            Beer(self, 0, window_width - 30, window_height / 2)
            Cannon(self, 45, window_width - 30, window_height / 2)
            Background(self)


    DaniilGame().run()