from gamelib import SpriteEntity, Game
from scene import Scene


class BackgroundImage(SpriteEntity):
    def __init__(self, game):
        super().__init__(game, "sd42.png", -1)
        self.bottom = 400
        self.left = game.height

    def update(self, dt):
        pass


class Arleen(Scene):
    def __init__(self, game: Game, x: int, y:int, width: int, height:int):
        self.game = game
        BackgroundImage(game)
        self.game.biertje = Bottle(game, "biertje.png", 1180, 690, 1, 0)
        Table(game, "table.png", 830, 450)
        Table(game, "table.png", 890, 470)
        Table(game, "table.png", 950, 500)
        Table(game, "table.png", 1010, 540)
        Table(game, "table.png", 1070, 590)
        Table(game, "table.png", 1130, 650)
        Table(game, "table.png", 1190, 720)
        self.game.monitors["monitor1"] = Monitor(game, "monitor1.png", 830, 430, 1.5, 1)
        self.game.monitors["monitor2"] = Monitor(game, "monitor1.png", 890, 455, 1.7, 1)
        self.game.monitors["monitor3"] = Monitor(game, "monitor2.png", 950, 485, 1.7, 1)
        self.game.monitors["monitor4"] = Monitor(game, "monitor2.png", 1010, 528, 1.8, 1)
        self.game.monitors["monitor5"] = Monitor(game, "monitor3.png", 1070, 582, 1.9, 1)
        self.game.monitors["monitor6"] = Monitor(game, "monitor3.png", 1130, 644, 2, 1)


class Bottle(SpriteEntity):
    def __init__(self, game, image_file, x, bottom, scale, rotation):
        super().__init__(game, image_file)
        self.x = x
        self.bottom = bottom
        self.scale = scale
        self.rotation = 4
        self.go = False

    def update(self, dt):
        if self.go:
            self.bottom += 2


class Monitor(SpriteEntity):
    def __init__(self, game, image_file, x, bottom, scale, rotation):
        super().__init__(game, image_file)
        self.x = x
        self.bottom = bottom
        self.scale = scale
        self.rotation = 6 if x == 830 else 0
        self.stop = False

    def update(self, dt):
        if self == self.game.monitors["monitor1"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 45:
                self.rotation = 0
                self.stop = True
                self.game.monitors["monitor2"].rotation = 4
        if self == self.game.monitors["monitor2"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 45:
                self.rotation = 0
                self.stop = True
                self.game.monitors["monitor3"].rotation = 4
        if self == self.game.monitors["monitor3"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 40:
                self.rotation = 0
                self.stop = True
                self.game.monitors["monitor4"].rotation = 4
        if self == self.game.monitors["monitor4"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 40:
                self.rotation = 0
                self.stop = True
                self.game.monitors["monitor5"].rotation = 4
        if self == self.game.monitors["monitor5"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 35:
                self.rotation = 0
                self.stop = True
                self.game.monitors["monitor6"].rotation = 4
        if self == self.game.monitors["monitor6"]:
            if self.rotation != 0 and not self.stop:
                self.x += self.rotation * dt
                self.rotation += self.rotation * dt
            if self.rotation >= 25:
                self.rotation = 0
                self.stop = True
                self.game.biertje.go = True



class Table(SpriteEntity):
    def __init__(self, game, image_file, x, top):
        super().__init__(game, image_file)
        self.x = x
        self.top = top

    def update(self, dt):
        pass
    

if __name__ == "__main__":
    class ArleenGame(Game):
        monitors = dict()
        biertje = None
        def __init__(self):
            super().__init__(1600, 800)

        def start(self):
            Arleen(self, 200, 200, 400, 400)
            
    ArleenGame().run()