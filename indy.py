from scene import Scene
from gamelib import Game, SpriteEntity
from datetime import datetime
import random

class Indy(Scene):
    def start():
        Peter(self.__game)

class Peter(SpriteEntity):
    def __init__(self, game: Game):
        super().__init__(game, "peter.png")
        self.start_time = datetime.now().timestamp()
        self.x = 280
        self.y = 200
        self.exe_event = False
        self.x_speed = 1
        self.y_speed = 1

    def update(self, dt):
        self.rotation+=5
        self.x+=self.x_speed
        self.y+=self.y_speed
        r = random.randint(0,2)
        
        if self.x>370:
            self.x_speed =-r
        if self.x<30:
            self.x_speed =r
        if self.y>370:
            self.y_speed = -r
        if self.y<30:
            self.y_speed = r
        if self.x_speed > 0:
            self.x_speed +=r
        else:
            self.x_speed -=r
        if self.y_speed > 0:
            self.y_speed +=r
        else:
            self.y_speed -=r

if __name__ == "__main__":
    class Main(Game):
        def __init__(self):
            super().__init__(400, 400)
        
        def start(self):
            Peter(self)
            
                
    Main().run()