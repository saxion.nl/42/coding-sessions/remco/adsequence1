from time import sleep
from gamelib import Game, SpriteEntity, LabelEntity
from math import sin, cos, radians, tan
import random
# import pyglet


class Cristi(Game):
    def start(self):

        # Create the bg
        self.bg = SpriteEntity(self, "background.png")
        self.bg.top = self.height

        # spawn biertje and position top-left 
        self.biertje = SpriteEntity(self, "biertje.png", layer=1)
        self.biertje.x = 33
        self.biertje.y = self.height

        # spawn stairs
        self.stairs = SpriteEntity(self, "assets/flipstairs.png")
        self.stairs.height = self.height
        self.stairs.x = self.stairs.width / 2 - 25
        self.stairs.y = self.height - self.stairs.height / 2 - 100
        self.stairs.rotation = 8

    def update(self, dt):
        # biertje_is_sliding = False
        gravity = 200
        biertje_radius = self.biertje.width / 2

        self.biertje.y -= gravity * dt

        # Check for collisions with stairs
        if self.biertje.collides_with(self.stairs, overlap=biertje_radius-60):  
    
            self.biertje_prev_x = self.biertje.x
            self.biertje_prev_y = self.biertje.y

            self.biertje.x += 200 * dt
            self.biertje.y -= 0.7 * dt

    def on_draw(self):
        # Clear the window and draw entities
        self.__window.clear()
        for entity in self._entities:
            entity._drawable.draw()



# Create an instance of the scene and run the game
scene = Cristi(width=400, height=400)
scene.run()






# class Cris(Scene):

#     def __init__(self):
#         for i in range(4):
#             path = Word(multiplier)

#     def run(self, game, x, y, width, height):
#         path = Word(multiplier)
            


# class BackgroundImage(SpriteEntity):
#     pass


# class Word(LabelEntity):
#     def __init__(self, multiplier, function=sin):
#         self.__multiplier = multiplier
#         self.__path = 0
#         self.__function = cos if not function else sin

#     def update(self, dt):
#         self.__path = 50 * (1 + self.__function(radians(dt*1)))
        

# n=0

# # for n in range(1, 40):
# while True:
#     path_1 = 50 * (1 + sin(radians(n*1)))
#     path_2 = 50 * (1 + cos(radians(n*3)))
#     path_3 = 50 * (1 + sin(radians(n*5)))
#     path_4 = 50 * (1 + cos(radians(n*7))) 


#     print("Associate".center(int(path_1)), "Degree".center(int(path_2)))
#     print("Sax".center(int(path_3)), "Ion".center(int(path_4)))
#     sleep(0.05)
#     n+=1
