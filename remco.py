from gamelib import SpriteEntity, Game
from scene import Scene
from arleen import Arleen
from indy import Indy

class Remco(Scene):

    def __init__(self, game: Game, x: int, y:int, width: int, height:int):
        OFFSET_X = 20
        OFFSET_Y = 20

        lx = x + OFFSET_X + Lamp.LAMP_WIDTH//2
        ly = y + height - OFFSET_Y
        l4 = FallingLamp(game, lx+3*Lamp.LAMP_WIDTH, ly, y+Lamp.LAMP_HEIGHT)
        l3 = Lamp(game, lx+2*Lamp.LAMP_WIDTH, ly, l4)
        l2 = Lamp(game, lx+1*Lamp.LAMP_WIDTH, ly, l3)
        l1 = Lamp(game, lx+0*Lamp.LAMP_WIDTH, ly, l2)
        self.teacher = Teacher(game, lx, ly - height//2, l1)

    def start(self):
        self.teacher.start()


class Teacher(SpriteEntity):
    def __init__(self, game, x, y, lamp):
        super().__init__(game, "remco.png")
        self.x = x
        self.y = y
        self.__speed = 0
        self.__lamp = lamp
        self.__bottom = y

    def start(self):
        self.__speed = 100

    def update(self, dt):
        self.y += dt * self.__speed
        if self.y + self.height > self.__lamp.y:
            self.__speed *= -1
            self.__lamp.start_rotating()
        if self.y < self.__bottom:
            self.__speed = 0


class Lamp(SpriteEntity):
    LAMP_WIDTH = 80
    LAMP_HEIGHT = 64

    def __init__(self, game, x, y, other = None):
        super().__init__(game, "lampwithcap.png", 1)
    
        self.x = x
        self.y = y
        self.__other = other
        self.__speed = 0
        self.__moving_x = 0
        self.__moving_y = 0


    def start_rotating(self):
        self.__speed = -25
        self.__moving_x = 10
        self.__moving_y = 25

    def update(self, dt):
        self.rotation += dt * self.__speed
        self.x += dt * self.__moving_x
        self.y += dt * -self.__moving_y
        if self.rotation < -60:
            if self.__other:
                self.__other.start_rotating()
                self.__other = None

        if self.rotation > 90 or self.rotation <-90:
            self.__speed *= -1
            self.__moving_x *= -1
        self.__moving_y = -self.rotation // 10


class FallingLamp(Lamp):
    def __init__(self, game, x, y, bottom):
        super().__init__(game, x, y, None)
        self.__moving_y = 0
        self.__bottom = bottom
        # self.__nextScene = nextScene

    def start_rotating(self):
        self.__moving_y = -100
    
    def update(self, dt):
        self.y += dt * self.__moving_y
        if self.y < self.__bottom:
            self.__moving_y = 0
            # self.__nextScene.start()


if __name__ == "__main__":
    class RemcoGame(Game):
        def __init__(self):
            super().__init__(1600, 800)
            self.bg = SpriteEntity(self, "background.png", layer=-32767, x=800,y=800,width=1600, height=800)


        def start(self):
            Remco(self, 0, 400, 400, 400).start()

            
    RemcoGame().run()