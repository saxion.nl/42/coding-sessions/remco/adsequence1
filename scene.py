from gamelib import Game


class Scene:
    def __init__(self, game: Game, nextScene):
        self.__game = game
        self.__nextScene = nextScene 

    def start(self):
        pass

    def finished(self):
        self.__nextScene.start()