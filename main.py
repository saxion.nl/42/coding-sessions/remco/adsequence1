from gamelib import SpriteEntity, LabelEntity, Game
from scene import Scene
from remco import Remco
from arleen import Arleen
from kay import Kay
from daniil import DaniilGame
from gertjan import gertjan
from indy import Indy


class Sequence(Game):
    indy = Indy(this, None)
    gertjan = Gertjan(this, indy, 400, 0, 400, 400)
    arleen = Arleen(this, 800, 400, 400, 400)
    remco = Remco(this, indy, 800, 400, 400, 400)

    remco.start()