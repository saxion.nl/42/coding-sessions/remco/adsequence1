from gamelib import SpriteEntity, Game
from scene import Scene


class Jenny(Scene):
    def __init__(self, game: Game, x: int, y:int, width: int, height:int):
        Penguin(game, x + width//2, y)


class Penguin(SpriteEntity):
    PENGUIN_SPEED = 500

    def __init__(self, game, x, y):
        super().__init__(game, "toucan.png")
    
        self.x = x
        self.y = y
        self.__x_speed = self.PENGUIN_SPEED
        self.__y_speed = self.PENGUIN_SPEED

    def update(self, dt):
        self.x += self.__x_speed * dt
        self.y += self.__y_speed * dt
    

if __name__ == "__main__":
    class JennyGame(Game):
        def __init__(self):
            super().__init__(1600, 800)

        def start(self):
            Jenny(self, 200, 200, 400, 400)
            
    JennyGame().run()