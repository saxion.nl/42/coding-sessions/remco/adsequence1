import random
from gamelib import *
from scene import Scene

class Kay(Scene):
    def __init__(self, game: Game, x: int, y:int, window_width: int, window_height:int):
        self.window_width = window_width
        self.window_height = window_height
        self.game = Game(window_width, window_height)
        self.background = SpriteEntity(self.game, "background.png", layer=1, x=window_width / 2, y=window_height / 2,
                                       width=window_width, height=window_height)
        self.conveyor_belts = [
            ConveyorBelt(self.game, 275, 325, window_width, 20, "brown"),
            ConveyorBelt(self.game, 125, 250, window_width, 20, "brown"),
            ConveyorBelt(self.game, 275, 175, window_width, 20, "brown"),
        ]
        self.beers = []
        self.start = False

    def add_beer(self):
        beer_width = 30
        beer_height = 40
        beer_speed = 2
        beer_x = self.window_width - beer_width
        beer_y = 400
        beer = Beer(self.game, beer_x, beer_y, beer_width, beer_height, beer_speed)
        self.beers.append(beer)

    def update(self, dt):
        # Move the beers
        
        beer_speed = 2
        for beer in self.beers:
            beer.move()
           

        # Remove beers that have passed the conveyor belt
        self.beers = [beer for beer in self.beers if beer.sprite.x < self.conveyor_belts[0].sprite.width]

    def start(self):
        self.game.update = self.update

        # Add initial beers
        for _ in range(1):
            self.add_beer()

        # Run the game
        self.game.run()

   

class Beer:
    def __init__(self, game, x, y, width, height, speed):
        self.sprite = SpriteEntity(game, "biertje.png", layer=3, x=x, y=y, width=width, height=height)
        self.speed = speed

    def move(self):
        # start
        if self.sprite.y > 350:
            self.sprite.y -= self.speed
        # drop right
        if self.sprite.x >= 350 and self.sprite.y > 200 and self.sprite.y < 275:
            self.sprite.y -= self.speed
            self.sprite.x += self.speed /2
        # move right
        if self.sprite.y < 275 and self.sprite.y > 270 :
            self.sprite.x += self.speed
        # move left
        if self.sprite.y == 350 or self.sprite.y == 200:
            self.sprite.x -= self.speed
        # drop left
        if self.sprite.x < 50 and self.sprite.y > 275:
            self.sprite.y -= self.speed
            self.sprite.x -= self.speed / 2
        if self.sprite.x < 50 and self.sprite.y < 201:
            self.sprite.y -= self.speed
            self.sprite.x -= self.speed / 2


class ConveyorBelt:
    def __init__(self, game, x, y, width, height, color):
        self.sprite = SpriteEntity(game, "conveyor.png", layer=2, x=x, y=y, width=width, height=height)
        self.color = color


class GameApp:
    def __init__(self, window_width, window_height):
        self.window_width = window_width
        self.window_height = window_height
        self.game = Game(window_width, window_height)
        self.background = SpriteEntity(self.game, "background.png", layer=1, x=window_width / 2, y=window_height / 2,
                                       width=window_width, height=window_height)
        self.conveyor_belts = [
            ConveyorBelt(self.game, 275, 325, window_width, 20, "brown"),
            ConveyorBelt(self.game, 125, 250, window_width, 20, "brown"),
            ConveyorBelt(self.game, 275, 175, window_width, 20, "brown"),
        ]
        self.beers = []


    def add_beer(self):
        beer_width = 30
        beer_height = 40
        beer_speed = 2
        beer_x = self.window_width - beer_width
        beer_y = 400
        beer = Beer(self.game, beer_x, beer_y, beer_width, beer_height, beer_speed)
        self.beers.append(beer)

    def update(self, dt):
        # Move the beers
        beer_speed = 2
        for beer in self.beers:
            beer.move()
           

        # Remove beers that have passed the conveyor belt
        self.beers = [beer for beer in self.beers if beer.sprite.x < self.conveyor_belts[0].sprite.width]

    def start(self):
        self.game.update = self.update

        # Add initial beers
        for _ in range(1):
            self.add_beer()

        # Run the game
        self.game.run()


if __name__ == "__main__":
    window_width = 400
    window_height = 400

    game_app = GameApp(window_width, window_height)
    game_app.start()
    # class KayGame(Game):
    #     def __init__(self):
    #         super().__init__(window_width, window_height)
    #     def start(self):
    #         app = Kay(self, 0, 400, 400, 400)
    #         p
    # KayGame().run()
        