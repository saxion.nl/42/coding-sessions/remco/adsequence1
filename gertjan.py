from scene import Scene
from gamelib import Game, SpriteEntity
from datetime import datetime

class Bear(SpriteEntity):
    def __init__(self, game: Game):
        super().__init__(game, "biertje.png")
        self.start_time = datetime.now().timestamp()
        self.sec_1_done = False
        self.sec_2_done = False
        self.sec_3_done = False
        self.sec_4_done = False
        self.x = 300  # Updated
        self.y = 100  # Updated
        self.rotation = 270
        
    def update(self, dt):
        if not self.sec_1_done:
            self.x -= 2  # Updated
                
        if self.x <= 100 and not self.sec_2_done:  # Updated
            self.sec_1_done = True
            self.rotation = 190
            self.y += 2  # Updated
            
        if self.y >= 300 and not self.sec_3_done:  # Updated
            self.sec_2_done = True
            self.rotation = 270
            self.x += 2  # Updated
            
        if self.x >= 300 and not self.sec_4_done:  # Updated
            self.sec_3_done = True
            self.rotation = 200
            self.sec_4_done = True
            
        if self.sec_4_done:
            self.y -= 2
            self.x -= 3  # Updated
        
class Table(SpriteEntity):
    def __init__(self, game: Game, x, y, rotation=0):
        super().__init__(game, "table.png")
        self.x = x
        self.y = y
        self.rotation = rotation
        
class Cannon(SpriteEntity):
    def __init__(self, game: Game):
        super().__init__(game, "kanon.png")
        self.start_time = datetime.now().timestamp()
        self.x = 380  # Updated
        self.y = 100  # Updated
        self.exe_event = False
        
    def start(self):
        Explosion(self.game)
        
        
class Explosion(SpriteEntity):
    def __init__(self, game: Game):
        super().__init__(game, "explosion.png")
        self.start_time = datetime.now().timestamp()
        self.exe_event = False
        self.x = 300  # Updated
        self.y = 130  # Updated
        self.width = 100  # Updated
        self.height = 100  # Updated
        
    def update(self, dt):
        if int(datetime.now().timestamp()) - self.start_time > 0.1 and not self.exe_event:
            Bear(self.game)
            self.remove()
            self.exe_event = True
      

# class Main(Game):
#     def __init__(self):
#         super().__init__(400, 400)  # Updated
    
#     def start(self):
#         SpriteEntity(self, "background.png", layer=-1, x=400 / 2, y=400 / 2, width=900, height=900)
#         Cannon(self)
#         Table(self, 95, 100, 85)  # Updated
#         Table(self, 95, 300, 100)  # Updated
#         Table(self, 300, 300, 260)  # Updated
        
class Gertjan(Scene):
    def __init__(self, game: Game, x: int, y:int, width: int, height:int):
        self.game = game
        
    
    def start(self):
        Table(self.game, 95, 100, 85)  # Updated
        Table(self.game, 95, 300, 100)  # Updated
        Table(self.game, 300, 300, 260)  # Updated
        Cannon(self.game).start()
        
        

if __name__ == "__main__":
    class RemcoGame(Game):
        def __init__(self):
            super().__init__(1600, 800)
            self.bg = SpriteEntity(self, "background.png", -32767)

        def start(self):
            Gertjan(self, 0, 400, 400, 400).start()

            
    RemcoGame().run()
